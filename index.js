var wwwContentData = {
		cells: 4,
		template: 'https://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/htm/index-dsgn-photos.html',
		device: 'D',
		joinLogin: 'N',
		manipulate: 'N',
		creatives:[[
			{	
				indx: 0,
				loc: 'https://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/media/lt-algn-PRTY-01.jpg'
				
			},
			{	
				indx: 0,
				loc: 'https://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/media/lt-algn-PRTY-02.jpg'
				
			}
		],
		[
			{	
				indx: 0,
				loc: 'https://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/media/lt-algn-LFSTYL-01.jpg'
				
			},
			{	
				indx: 0,
				loc: 'https://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/media/lt-algn-LFSTYL-02.jpg'
				
			}
		],
		[
			{	
				indx: 0,
				loc: 'https://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/media/rt-algn-TRDTNL-01.jpg'
				
			},
			{	
				indx: 0,
				loc: 'https://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/media/rt-algn-TRDTNL-02.jpg'
				
			}
		],
		[
			{	
				indx: 0,
				loc: 'https://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/media/rt-algn-FUN-01.jpg'
				
			},
			{	
				indx: 0,
				loc: 'https://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/media/rt-algn-FUN-02.jpg'
				
			}
		]
	]
};
var mdModule = require('mobile-detect');
/*
 * 
 */
function processRequest(request, callback) {
	var md = new mdModule(request.headers['user-agent']);
	var deviceType = "D";
	if(md.mobile() != null) {
		deviceType = "M"
	} else if(md.tablet() != null) {
		deviceType = "T"
	}
	console.log("URL: " + request.info.host + ", Device: : " + deviceType);
	
		
	var host = request.info.host;
	//host = "aakearn";
	
	if (host.indexOf('aakmatri') != -1) {
		
		wwwContentData.joinLogin = 'Y';
		wwwContentData.manipulate = 'Y';
		
		if(deviceType === 'M') {
			wwwContentData.template = 'http://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/htm/index-dsgn-1pic-Mb.html';
			wwwContentData.cells = 1;
			wwwContentData.device = 'M';
		} else if (deviceType === 'D') {
			wwwContentData.template = 'http://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/htm/index-dsgn-photos.html';
			wwwContentData.cells = 4;
			wwwContentData.device = 'D';
		} else if (deviceType === 'T') {
			wwwContentData.template = 'http://s3.ap-south-1.amazonaws.com/akstc/www/ppl/matri/htm/index-dsgn-photos.html';
			wwwContentData.cells = 4;
			wwwContentData.device = 'T';
		}
		
	} else if (host.indexOf('digital') != -1) {
		
		wwwContentData.template = 'http://s3.ap-south-1.amazonaws.com/akstc/brnd/corp-site/index-corp-site.html';
		wwwContentData.device = deviceType;
		
	} else if (host.indexOf('aakjobs') != -1) {
		
		wwwContentData.template = 'http://s3.ap-south-1.amazonaws.com/akstc/www/ppl/jobs/aakjob.html';
		wwwContentData.device = deviceType;

	} else if (host.indexOf('aakblog') != -1) {
		
		wwwContentData.template = 'http://s3.ap-south-1.amazonaws.com/akstc/www/ppl/blog/aakblog.html';
		wwwContentData.device = deviceType;
		wwwContentData.joinLogin = 'Y';
	}
	else if (host.indexOf('aakreach') != -1) {
		
		wwwContentData.template = 'http://s3.ap-south-1.amazonaws.com/akstc/www/reach/htm/aakreach.html';
		wwwContentData.device = deviceType;
	}
	else if (host.indexOf('aakbiz') != -1) {
		
		wwwContentData.template = 'http://s3.ap-south-1.amazonaws.com/akstc/www/org/stack/aakbiz.html';
		wwwContentData.device = deviceType;
	}
	else if (host.indexOf('aakdrive') != -1) {
		
		wwwContentData.template = 'http://s3-ap-southeast-1.amazonaws.com/aak-w-cmn/www/cmn/drive/ppl-drive.html';
		wwwContentData.device = deviceType;
	}
	else if (host.indexOf('aakearn') != -1) {
		
		wwwContentData.template = 'http://s3-ap-southeast-1.amazonaws.com/aakearn.online/aak-earn-online.html';
		wwwContentData.device = deviceType;
	}
	callback(wwwContentData);  
}   
module.exports.process = processRequest;