var mdModule = require('mobile-detect');

var indxDynaDataProvider = require('./index.js');

module.exports  = [{
    	method: 'GET',
    	path : '/',
		handler : function(request, reply) {
			indxDynaDataProvider.process(request, function(response) {
    			reply.view('www_landing', response);
        	});
		}
	},
	{
		method: 'GET',
	    path: '/health',
	    handler: function (request, reply) {   
	        reply('k');
	    }
   },
   {
        method: 'GET',
        path: '/aaksearchedhistory',
        handler: function (request, reply) {
        reply.view('searchedhistory'); 
        }
},
{
        method: 'POST',
        path: '/aaksearchedhistory',
        handler: function (request, reply) {
            var cntxtArrayLength=JSON.parse(request.payload.JsonObj).cntxtArray.length;
            if(cntxtArrayLength!=0){
        reply.view('searchedhistory',JSON.parse(request.payload.JsonObj));
        }
        else
        {
         reply.view('searchedhistory',{});   
        } 
        }
}];